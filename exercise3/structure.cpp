#include <iostream>
#include <string>

using  namespace std;

struct Employee {

  string first_name;
  string last_name;
  string id;
  string email;
  string phone_number;
  int salary;
  Employee *manager;
  
};

void report(Employee s){


  cout << "Employees manager is: " << s.manager -> first_name << endl;
}
int main()
{

  Employee accountant;
  Employee admin;
  Employee mng;


  accountant.first_name =" Edoardo";
  accountant.last_name = "Fratantonio";
  accountant.id = "CDDDSFG7733";
  accountant.email ="edofrata@gmail.com";
  accountant.phone_number = "00887383920";
  accountant.salary = 1000000000;
  accountant.manager = &mng;

  admin.first_name =" Matt";
  admin.last_name = "Donovan";
  admin.id = "CCCTTSFG5789";
  admin.email = "mattdonovan@gmail.com";
  admin.salary = 20000;
  admin.manager = &mng;

  mng.first_name = "Tizio";
  mng.last_name = "Caio";
  mng.id = "CRCHBHJB4627829";
  mng.email = "tiziocaio@gmail.com";
  mng.salary = 478880;

  report(accountant);
}
  
