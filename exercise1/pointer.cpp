#include <iostream>

using namespace std;

int main()
{
  

  double number = 2.4;


  double *d_pointer;

  d_pointer = &number;

  cout << "Variable value: " << number << "; address: " << &number << endl;

  cout << "Pointer value : " << d_pointer << "; The address is: " << &d_pointer << "; deferenced: " << *d_pointer << endl;

  
}
